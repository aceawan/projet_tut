<?php

namespace projet_tut\views;

use Illuminate\Database\QueryException;
use projet_tut\models\Client;
use projet_tut\models\Reservation;
use projet_tut\models\Vehicule;
use projet_tut\views\VueVisiteur;
use Slim\Slim;


class VueMembre
{

    public function render($i)
    {
        session_start();

        $app = \Slim\Slim::getInstance();
        $path_base = $app->urlFor("baseMembre");
        $path = $app->request->getRootUri();

        if (isset($_SESSION['usermail'])) {

            switch ($i) {
                case 0:
                    $content = $this->accueil();
                    break;
                case 1:
                    $content = $this->confirm();
                    break;
                case 2:
                    $content = $this->planning();
                    break;
                case 3:
                    $content = $this->form_reservation();
                    break;
                case 4:
                    $content = $this->liste_reservation();
                    break;
            }
        } else {
            $content = "You are not logged in.";
        }

        echo <<<END
        <!DOCTYPE html>
        <html>
        <head>
            <title>AutoVoiture</title>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="$path/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="$path/bower_components/bootstrap-calendar/css/calendar.css">
            <link href="$path/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	        <link href="$path/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
            <link href="css/base.css" rel="stylesheet">
        </head>
        <body>
            <nav>
                <ul>
                </ul>
            </nav>

           <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="$path_base">AutoVoiture</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="$path_base">Home</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


            $content

    </div>

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <!--script src="js/bootstrap.min.js"></script-->

        </body>
        </html>
END;
    }


    public function accueil()
    {
        $app = \Slim\Slim::getInstance();
        $path_deco = $app->urlFor("deco");
        $path_planning = $app->urlFor("planning");
        $path_reservation = $app->urlFor("reservation");
        $path_liste_reservation = $app->urlFor("liste_reservation");

        $user = \projet_tut\models\Client::find($_SESSION['usermail']);
        $prenom = $user->prenom;
        $nom = $user->nom;
        $result = <<<END

<div class="centered">

<p>Bienvenue $nom $prenom</p><br/>

<a href="$path_deco" class="btn btn-primary" role="button">Déconnexion</a>
<a href="$path_planning" class="btn btn-primary" role="button">Consulter le planning</a>
<a href="$path_liste_reservation" class="btn btn-primary" role="button">Lister mes réservations</a>
<a href="$path_reservation" class="btn btn-primary" role="button">Réserver un véhicule</a>

</div>
END;
        return $result;
    }


    public function confirm()
    {
        return <<<END
        <p>Merci pour votre confirmation.</p>
END;
    }

    public function planning()
    {
        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();

        if (isset($_SESSION['usermail'])) {
            return <<<END

  <div class="container">

	<div class="page-header">

	  <div class="pull-right form-inline">
		<div class="btn-group">
		  <button class="btn btn-primary" data-calendar-nav="prev"><< Avant</button>
																	  <button class="btn" data-calendar-nav="today">Aujourd'hui</button>
																	  <button class="btn btn-primary" data-calendar-nav="next">Après >></button>
		</div>
		<div class="btn-group">
		  <button class="btn btn-warning" data-calendar-view="year">Année</button>
		  <button class="btn btn-warning active" data-calendar-view="month">Mois</button>
		  <button class="btn btn-warning" data-calendar-view="week">Semaines</button>
		  <button class="btn btn-warning" data-calendar-view="day">Jour</button>
		</div>
	  </div>
	  <h3></h3>
	</div>
	
	

	<script type="text/javascript" src="$path/bower_components/jquery/jquery.js"></script>
	<script type="text/javascript" src="$path/bower_components/underscore/underscore.js"></script>
	<script type="text/javascript" src="$path/bower_components/bootstrap/dist/js/bootstrap.js"></script>

	<script type="text/javascript" src="$path/bower_components/bootstrap-calendar/js/language/fr-FR.js"></script>
	<script type="text/javascript" src="$path/bower_components/bootstrap-calendar/js/calendar.js"></script>


	<script type="text/javascript" src="$path/bower_components/moment/moment.js"></script>
	<div id="calendar">
        <script type="text/javascript" src="$path/js/cal.js"></script>
	</div>

  </div>
  
END;
        }
    }

    public function form_reservation()
    {
        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();

        if (isset($_SESSION['usermail'])) {
            $result = <<<END

		<div class="container" style="width: 500px;">
				<h2>Réservez votre véhicule</h2>
				<span>Date de prise en main du véhicule :</span>
				<div class='input-group date' id='start'>
						<input type='text' class="form-control" />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						</span>
				</div>
				<span>Date du retour du véhicule :</span>
				<div class='input-group date' id='stop'>
						<input type='text' class="form-control" />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						</span>
				</div>
				<span>Choisissez un véhicule :</span>
				<div id='vehicule'>
                        <input type='number' class="form-control" />
				</div>
<p></p>
        <a class="btn btn-lg btn-primary btn-block" id="send_info">Faire la demande</a>
		</div>

	<script type="text/javascript" src="$path/bower_components/jquery/jquery.js"></script>
    <script src="$path/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="$path/bower_components/moment/moment.js"></script>
		<script src="$path/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript">
				$(function () {
                        $('#send_info').click(function(){
                            // this is the date as a string
                            var vehicule = $("#vehicule").find("input").val();
                            var date_courante = Date.now();

                            //this is the date as a unix timestamp, much simpler to use, wow, such timestamp
                            var start = $("#start").data("DateTimePicker").date(); 
                            var stop = $("#stop").data("DateTimePicker").date();


                            if(start != null && stop != null){
                                if(start < stop && start >= date_courante){
                                   
                                $.ajax({
                                    url:"http://localhost/projet_tut/reservation-send",
                                    type:"POST",
                                    data : 'start=' + start + '&stop=' + stop + '&vehicule=' + vehicule,
                                    dataType : 'html',
                                    success: function(res) {
                                        alert(res);
                                    },
                                    error: function() {
                                        alert(res);
                                    }    
                                });
                                //window.location.href =  "http://localhost$path/reservation-send";
                                }else{
                                    alert("Nous ne gérons pas encore la réservation dans le passé !");
                                }
                            } else{
                                alert("Vous devez indiquer une date valide");
                            }
                        });

						$('#start').datetimepicker({
                            format: "DD/MM/YYYY-HH:mm"
                        });
						$('#stop').datetimepicker({
                            format: "DD/MM/YYYY-HH:mm"
                        });
				});
		</script>


END;
            return $result;
        }
    }

    public function liste_reservation(){
        $app = Slim::getInstance();
        $path = $app->urlFor("suppr_reservation");

        $reservation = Reservation::where('email', '=', $_SESSION['usermail'])->get();
        $result = <<<END
                    <br><br><br>
END;
        if (empty($reservation[0])){
            $result .= <<<END
                        <div class="row">
                        <div class="col-xs-6">Vous n'avez aucune réservation en cours</div></div>
END;
        } else {
            foreach ($reservation as $r) {
                $path_supprimer = $path . "?id=" . $r->id;
                $vehicule = Vehicule::find($r->num_vehicule);
                $result .= <<<END
<div class="row">
<div class="col-xs-2">Véhicule réservé : <br>$vehicule->type</div>
<div class="col-xs-2">Date de début : <br>$r->start</div>
<div class="col-xs-2">Date de fin : <br>$r->stop </div>
<div class="col-xs-2"><a class="btn btn-lg btn-primary" href="$path_supprimer" id="button_suppr">Supprimer</a></div>
<br>

END;
            }
        }
        return $result;
    }
}


