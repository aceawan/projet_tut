<?php

namespace projet_tut\views;

use Illuminate\Database\QueryException;
use projet_tut\models\Client;
use projet_tut\models\Reservation;
use projet_tut\models\Vehicule;
use projet_tut\views\VueVisiteur;
use Slim\Slim;


class VueAdmin {

	public function render($i){
        session_start();

        $app = \Slim\Slim::getInstance();
        $path_base = $app->urlFor("baseAdmin");
        $path = $app->request->getRootUri();
        
        if(isset($_SESSION['usermail'])){
            if($_SESSION['usermail'] == 'admin@autovoiture.fr'){
                
                switch($i){
                case 0:
                    $content = $this->accueil();
                    break;
                case 1:
                    $content = $this->demandes();
                    break;
                case 2:
                    $content = $this->gestion_vehicule();
                    break;
                case 3:
                    $content = $this->liste_vehicule();
                    break;
                case 4:
                    $content = $this->form_ajout_vehicule();
                    break;
                case 5:
                    $content = $this->liste_vehicule_revision();
                    break;
                case 6:
                    $content = $this->liste_membre();
                    break;
                }
            }else{
                $app->redirect($app->urlFor("baseMembre"));
            }
        }else{
            $content = "You are not logged in yet.";            
        }

        
        echo <<<END
        <!DOCTYPE html>
        <html>
        <head>
            <title>AutoVoiture</title>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="$path/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="$path/bower_components/bootstrap-calendar/css/calendar.css">
            <link href="$path/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	        <link href="$path/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
            <link href="css/base.css" rel="stylesheet">
        </head>
        <body>
            <nav>
                <ul>
                </ul>
            </nav>

           <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="$path_base">AutoVoiture</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="$path_base">Home</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


            $content

    </div>

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <!--script src="js/bootstrap.min.js"></script-->

        </body>
        </html>
END;
    }


    public function accueil(){
        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();
        $path_insc = $app->urlFor("insc");
        $path_deco = $app->urlFor("deco");
        $path_planning = $app->urlFor("planning");
        $path_auth = $app->urlFor("auth");
        $path_reservation = $app->urlFor("reservation");
        $path_demandes = $app->urlFor("demandes");
        $path_gestion_vehicule = $app->urlFor("gestion_vehicule");
        $path_gestion_client = $app->urlFor("gestion_client");

        
        if(!isset($_SESSION['usermail'])){
            $result = <<<END

        <form class="form-signin" action="$path_auth" method="post">
            <h2 class="form-signin-heading">Connexion</h2>
            <label for="inputEmail" class="sr-only">Email</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email"required autofocus>
            <label for="inputPassword" class="sr-only">Mot de passe</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="mdp" required>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
         <a href="$path_insc" class="btn btn-lg btn-primary btn-block" role="button">Inscription</a>
      </form>

END;

        }
        else{
            $user = \projet_tut\models\Client::find($_SESSION['usermail']);
            $prenom = $user->prenom;
            $nom = $user->nom;
            $result = <<<END

<div class="centered">

<p>Bienvenue $nom $prenom</p><br/>

<a href="$path_deco" class="btn btn-primary" role="button">Déconnexion</a>
<a href="$path_planning" class="btn btn-primary" role="button">Consulter le planning</a>
<a href="$path_demandes" class="btn btn-primary" role="button">Consulter les demandes</a>
<a href="$path_gestion_vehicule" class="btn btn-primary" role="button">Gestion de véhicule</a>
<a href="$path_gestion_client" class="btn btn-primary" role="button">Gestion des membres</a>
</div>
END;
        }

        return $result;
    }



    public function demandes(){
        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();
        $path_valide = $app->urlFor("valide");
        $path_refuse = $app->urlFor("refuse");
        $demandes = Reservation::where('valide', '=', null) -> get();
        
        if(isset($_SESSION['usermail'])){
            $result = <<<END
<br>
END;

            
            foreach($demandes as $d){
                $p_valide = $path_valide . "?email=" . $d->email . "&id=" . $d->id;
                $p_refuse = $path_refuse . "?email=" . $d->email . "&id=" . $d->id;
                $result .= <<<END
<div class="row">
<div class="col-xs-3">Email : <br> $d->email</div>
<div class="col-xs-3">Début : $d->start <br>Fin : $d->stop</div>
<div class="col-xs-3">Véhicule : <br> qs </div>
<div class="col-xs-3"><a class="btn btn-lg btn-primary" href=$p_valide id="button_valide">Valider</a> <a class="btn btn-lg btn-primary" href=$p_refuse id="button_refuser">Refuser</a></div>
</div>
<br>




END;
            }

            $result .= <<<END
END;

            
            return $result;
        }
    }

    public function gestion_vehicule(){
        if($_SESSION['usermail'] == 'admin@autovoiture.fr'){
            
            $app = \Slim\Slim::getInstance();
            $path = $app->request->getRootUri();
            $path_vehicule_ajout = $app->urlFor("gestion_vehicule_form_ajout");
            $path_vehicule_liste = $app->urlFor("gestion_vehicule_liste");
            $path_vehicule_liste_revision = $app->urlFor("gestion_vehicule_liste_revision");
            $result = <<<END

<div class="centered">
<a href="$path_vehicule_liste" class="btn btn-primary" role="button">Liste des véhicules</a>
<a href="$path_vehicule_liste_revision" class="btn btn-primary" role="button">Liste des véhicules en révision</a>
<a href="$path_vehicule_ajout" class="btn btn-primary" role="button">Ajouter un véhicule</a>


</div>
END;

            return $result;
        }
    }

    public function liste_vehicule(){
        if($_SESSION['usermail'] == 'admin@autovoiture.fr'){
            $app = Slim::getInstance();
            $path_suppr = $app->urlFor("gestion_vehicule_supprimer");
            $path_stat = $app->urlFor("gestion_vehicule_changer_statut");

            $vehicules = Vehicule::where('revision', '=', 0)->get();
            $result = <<<END
                    <br><br><br>
END;
            if(empty($vehicules[0])){
                $result .= <<<END
                        <div class="row">
                        <div class="col-xs-6">Il n'y a aucun véhicule disponible pour le moment</div></div>
END;
            }else{
                foreach($vehicules as $v) {
                    $path_supprimer = $path_suppr . "?id=" . $v->num_vehicule;
                    $path_statut= $path_stat . "?id=" . $v->num_vehicule;
                    $result .= <<<END
<div class="row">
<div class="col-xs-2">Type : <br>$v->type</div>
<div class="col-xs-2">Age : <br>$v->age </div>
<div class="col-xs-2">Nombres de chauffeurs : <br>$v->nb_chauffeur </div>
<div class="col-xs-2">Nombres de kilomètres : <br>$v->nb_km </div>
<div class="col-xs-2">km avant révision : <br>$v->km_revision</div>
<div class="col-xs-2"><a class="btn btn-lg btn-primary" href="$path_supprimer" id="button_suppr">Supprimer</a> <a class="btn btn-lg btn-primary" href="$path_statut" id="button_revision">Révision</a></div></div>
<br>

END;
                }
            }

            return $result;
        }
    }

    public function liste_vehicule_revision(){
        if($_SESSION['usermail'] == 'admin@autovoiture.fr'){

            $app = Slim::getInstance();
            $path_suppr = $app->urlFor("gestion_vehicule_supprimer_rev");
            $path_stat = $app->urlFor("gestion_vehicule_changer_statut_rev");

            $veh = Vehicule::where('revision', '=', 1)->get();
            $result = <<<END
                    <br><br><br>
END;

            if(empty($veh[0])){
                $result .= <<<END
                <div class="row">
                <div class="col-xs-6">Il n'y a aucun véhicule en révision en ce moment</div></div>
END;
            }else {
                foreach ($veh as $v) {
                    $path_supprimer = $path_suppr . "?id=" . $v->num_vehicule;
                    $path_statut= $path_stat . "?id=" . $v->num_vehicule;
                    $result .= <<<END
<div class="row">
<div class="col-xs-2">Type : <br>$v->type</div>
<div class="col-xs-2">Age : <br>$v->age </div>
<div class="col-xs-2">Nombres de chauffeurs : <br>$v->nb_chauffeur </div>
<div class="col-xs-2">Nombres de kilomètres : <br>$v->nb_km </div>
<div class="col-xs-2">km avant révision : <br>$v->km_revision</div>
<div class="col-xs-2"><a class="btn btn-lg btn-primary" href="$path_supprimer" id="button_suppr">Supprimer</a> <a class="btn btn-lg btn-primary" href="$path_statut" id="button_revision">Disponible</a></div></div>
<br>

END;
                }
            }

            return $result;
        }
    }


    public function form_ajout_vehicule(){
        if($_SESSION['usermail'] == 'admin@autovoiture.fr'){

            $app = Slim::getInstance();
            $path = $app->urlFor('gestion_vehicule_ajout');
            $result= <<<END
                     <div class="container" style="width: 500px;"><br><br><br>
                        <form class="form-signin" action="$path" method="post">
                            <h2 class="form-signin-heading">Ajout d'un véhicule</h2>

                            <label class="sr-only">Type du véhicule</label>
                            <input type="text" id="type" class="form-control" placeholder="Type du véhicule" name="type" required>

                            <label class="sr-only">Nombres de kilomètres du véhicule</label>
                            <input type="number" id="nb_km" class="form-control" placeholder="Nombres de kilomètres" name="nb_km" min="0" required>

                            <label class="sr-only">Nombres de chauffeurs</label>
                            <input type="number" id="nb_chauffeur" class="form-control" placeholder="Nombres de chauffeurs" name="nb_chauffeur" min="1" required>

                            <label class="sr-only">Nombres de kilomètres avant révision</label>
                            <input type="number" id="km_revision" class="form-control" placeholder="Nombres de kilomètres avant révision" name="km_revision" min="0" required>

                            <label class="sr-only">Age du véhicule</label>
                            <input type="number" id="age" class="form-control" placeholder="Age du véhicule" name="age" min="0" required>

                            <button class="btn btn-lg btn-primary btn-block" type="submit">Ajouter ce véhicule</button>
                        </form>
                     </div>
END;
        }

        return $result;
    }


    public function liste_membre(){
        if($_SESSION['usermail'] == 'admin@autovoiture.fr'){
            $app = Slim::getInstance();
            $clients = Client::all();
            $path_suppr = $app->urlFor("gestion_client_supprimer");
    
            $result = <<<END
                    <br><br><br>
END;
            foreach($clients as $c) {
                $path_supprimer = $path_suppr . "?email=" . $c->email;
 
                //$path_supprimer = $path_suppr . "?id=" . $c->num_vehicule;
                //$path_statut= $path_stat . "?id=" . $v->num_vehicule;
                $result .= <<<END
<div class="row">
<div class="col-xs-2">Nom : <br>$c->nom</div>
<div class="col-xs-2">Prénom : <br>$c->prenom</div>
<div class="col-xs-2">Téléphone : <br>$c->tel </div>
<div class="col-xs-2">Permis : <br>$c->permis </div>
<div class="col-xs-2">Email : <br>$c->email</div>
<div class="col-xs-2"><a class="btn btn-lg btn-primary" href="$path_supprimer" id="button_suppr">Supprimer</a></div></div>
<br>

END;
            }
            return $result;
        }
    }



        
}