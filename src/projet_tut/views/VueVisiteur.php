<?php

namespace projet_tut\views;

use Illuminate\Database\QueryException;
use projet_tut\models\Client;
use projet_tut\models\Reservation;
use projet_tut\views\VueVisiteur;


class VueVisiteur{

	public function render($i){
        session_start();

        $app = \Slim\Slim::getInstance();
        $path_base = $app->urlFor("base");
        $path = $app->request->getRootUri();

        switch($i){
        case 0:
            $content = $this->accueil();
            break;
        case 1:
            $content = $this->form_insc();
            break;
        }


        /*
          if(isset($_SESSION['userid'])){
          if(isset($_SESSION['admin'])){
          if($_SESSION['admin']){
          $userid = $_SESSION['userid'];
          $path_client = $app->urlFor('admin', array('id' => $userid));
          $line = "<a href=\"$path_client\">Mon profil</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a><a href=\"$path_caisse\">Caisse</a><a href=\"$path_leconCommande\">Lecons</a><a href=\"$path_deco\">Deconnexion</a>";
                    
          }
          }
          else {
                
          $userid = $_SESSION['userid'];
          $path_client = $app->urlFor("client", array('id' => $userid));
                
          $line = "<a href=\"$path_client\">Mon profil</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a><a href=\"$path_leconCommande\">Lecons</a><a href=\"$path_deco\">Deconnexion</a>";
          }
          }
          else{
          $line = "<a href=\"$path_auth\">Authentification</a><a href=\"$path_insc\">Inscription</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a>";

          }
        */

        echo <<<END
        <!DOCTYPE html>
        <html>
        <head>
            <title>AutoVoiture</title>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="$path/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="$path/bower_components/bootstrap-calendar/css/calendar.css">
            <link href="$path/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	        <link href="$path/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
            <link href="css/base.css" rel="stylesheet">
        </head>
        <body>
            <nav>
                <ul>
                </ul>
            </nav>

           <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="$path_base">AutoVoiture</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="$path_base">Home</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


            $content

    </div>

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <!--script src="js/bootstrap.min.js"></script-->

        </body>
        </html>
END;
    }


    public function accueil(){
        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();
        $path_insc = $app->urlFor("insc");
        $path_auth = $app->urlFor("auth");

        $result = <<<END

        <form class="form-signin" action="$path_auth" method="post">
            <h2 class="form-signin-heading">Connexion</h2>
            <label for="inputEmail" class="sr-only">Email</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email"required autofocus>
            <label for="inputPassword" class="sr-only">Mot de passe</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="mdp" required>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
         <a href="$path_insc" class="btn btn-lg btn-primary btn-block" role="button">Inscription</a>
      </form>

END;


        return $result;
    }

    public function form_insc(){
        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();

        $path_insc = $app->urlFor("insc");

        $result= <<<END
        <form class="form-signin" action="$path_insc" method="post">
            <h2 class="form-signin-heading">Inscription</h2>
            <label for="nom" class="sr-only">Nom</label>
            <input type="text" id="inputPassword" class="form-control" placeholder="Nom" name="nom" required>
            <label for="nom" class="sr-only">Prénom</label>
            <input type="text" id="inputPassword" class="form-control" placeholder="Prénom" name="prenom" required>
            <label for="inputEmail" class="sr-only">Email</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email"required autofocus>
            <label for="inputPassword" class="sr-only">Mot de passe</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
            <label for="tel" class="sr-only">Téléphone</label>
            <input type="text" id="inputPassword" class="form-control" placeholder="N° tel" name="tel" required>
            <label for="meris" class="sr-only">Permis</label>
            <input type="text" id="inputPassword" class="form-control" placeholder="Permis" name="meris" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Inscription</button>
      </form>
END;

        return $result;
    }
    
}


