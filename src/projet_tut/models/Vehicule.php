<?php

namespace projet_tut\models;

class Vehicule extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'vehicule';
    protected $primaryKey = 'num_vehicule';
    public $timestamps = false;
    
}