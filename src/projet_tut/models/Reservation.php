<?php

namespace projet_tut\models;

class Reservation extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'reservation';
    protected $primaryKey = 'id';
    public $timestamps = false;

}