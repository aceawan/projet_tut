<?php

namespace projet_tut\models;

class Client extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'client';
    protected $primaryKey = 'email';
    public $timestamps = false;

}