<?php

namespace projet_tut\control;

use Illuminate\Database\QueryException;
use projet_tut\models\Client;
use projet_tut\models\Reservation;
use projet_tut\views\VueVisiteur;
use projet_tut\views\VueAdmin;
use projet_tut\models\Vehicule;
use Slim\Slim;

class AdminController extends AbstractController {

    
    public function index(){
        $vue = new VueAdmin();
        $vue->render(0);            
    }
  
    public function demandes(){
        $vue = new VueAdmin();
        $vue->render(1);
    }
    
    public function valide(){
        $app = \Slim\Slim::getInstance();
        $vue = new VueVisiteur();
        $email= filter_var($app->request->get('email'),FILTER_SANITIZE_STRING);
        $id= filter_var($app->request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        //echo $email;
        //echo $id;
        $demandes = Reservation::where('valide', '=', '1') -> get();

        $r = Reservation::where('id', '=', $id) -> get() -> first();
        $r-> valide = true;
       
        $file = "events.json.php";
        //$json = $json_decode(file_get_contents($file), true);
        $json = array();

        foreach($demandes as $d){
            //$v = Vehicule::where('')
            $json[] = array(
                'id' => $d->id,
                'title' => $d->email,
                'class' => 'event-important',
                'start' => $d->start,
                'end' => $d->stop,
            );
            
        }

        $r-> save();


        file_put_contents($file, json_encode(array('success' => 1, 'result' => $json)));
        
        $app->redirect($app->urlFor("demandes"));

        
        //$vue->render(5);
    }

    public function refuse(){
        $app = \Slim\Slim::getInstance();
        $vue = new VueVisiteur();
        $email= filter_var($app->request->get('email'),FILTER_SANITIZE_STRING);
        $id= filter_var($app->request->get('id'),FILTER_SANITIZE_NUMBER_INT);
        //echo $email;
        $r = Reservation::where('id', '=', $id) -> get() -> first();
        $r->valide = False;
        $r->save();

        $app->redirect($app->urlFor("demandes"));
        
        
        //$vue->render(5);
    }

    public function gestion_vehicule(){
        $vue = new VueAdmin();
        $vue->render(2);        
    }

    public function liste_vehicule(){
        $vue = new VueAdmin();
        $vue->render(3);                
    }

    public function liste_vehicule_revision(){
        $vue = new VueAdmin();
        $vue->render(5);
    }

    public function supprimerVehicule(){
        $app = Slim::getInstance();
        $url = $app->urlFor("gestion_vehicule_liste");
        $id = filter_var($app->request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $vehicule = Vehicule::find($id);
        if(!is_null($vehicule)) {
            $vehicule->delete();
        }
        $app->redirect($url);
    }

    public function changerStatut(){
        $app = Slim::getInstance();
        $url = $app->urlFor("gestion_vehicule_liste");
        $id = filter_var($app->request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $vehicule = Vehicule::find($id);
        var_dump($vehicule);
        if(!is_null($vehicule)) {
            if($vehicule->revision == 1){
                $vehicule->revision = 0;
            }else{
                $vehicule->revision = 1;
            }
        }
        $vehicule->save();
        $app->redirect($url);
    }

    public function supprimerVehicule_rev(){
        $app = Slim::getInstance();
        $url = $app->urlFor("gestion_vehicule_liste_revision");
        $id = filter_var($app->request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $vehicule = Vehicule::find($id);
        if(!is_null($vehicule)) {
            $vehicule->delete();
        }
        $app->redirect($url);
    }

    public function changerStatut_rev(){
        $app = Slim::getInstance();
        $url = $app->urlFor("gestion_vehicule_liste_revision");
        $id = filter_var($app->request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $vehicule = Vehicule::find($id);
        var_dump($vehicule);
        if(!is_null($vehicule)) {
            if($vehicule->revision == 1){
                $vehicule->revision = 0;
            }else{
                $vehicule->revision = 1;
            }
        }
        $vehicule->save();
        $app->redirect($url);
    }

    public function ajout_vehicule(){
        $app = \Slim\Slim::getInstance();
        $path = $app->urlFor("gestion_vehicule");

        $v = new Vehicule();
        $v->nb_km = filter_var($app->request->post('nb_km'), FILTER_SANITIZE_NUMBER_INT);
        $v->nb_chauffeur = filter_var($app->request->post('nb_chauffeur'), FILTER_SANITIZE_NUMBER_INT);
        $v->km_revision = filter_var($app->request->post('km_revision'), FILTER_SANITIZE_NUMBER_INT);
        $v->age = filter_var($app->request->post('age'), FILTER_SANITIZE_NUMBER_INT);
        $v->type = filter_var($app->request->post('type'), FILTER_SANITIZE_STRING);
        $v->revision = 0;
        $v -> save();

        $app->redirect($path);
    }

    public function form_ajout_vehicule(){
        $vue = new VueAdmin();
        $vue->render(4);
    }

    
    public function gestion_client(){
        $vue = new VueAdmin();
        $vue->render(6);        
    }

    public function supprimer_client(){
        $app = Slim::getInstance();
        $url = $app->urlFor("gestion_client");
        $email = filter_var($app->request->get('email'),FILTER_SANITIZE_STRING);
        
        $client = Client::find($email);
        if(!is_null($client)) {
            $client->delete();
        }        
        $app->redirect($url);
    }


}
