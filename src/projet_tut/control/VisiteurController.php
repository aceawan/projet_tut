<?php

namespace projet_tut\control;

use Illuminate\Database\QueryException;
use projet_tut\models\Client;
use projet_tut\models\Reservation;
use projet_tut\views\VueVisiteur;

class VisiteurController extends AbstractController {
    
    public function index(){
        $vue = new VueVisiteur();
        $vue->render(0);                 
    }
    
    public function form_insc(){
        $vue = new VueVisiteur();
        $vue->render(1);
    }


    public function inscription(){
        $app = \Slim\Slim::getInstance();
        $c = new Client();

        $path = $app -> request -> getRootUri();

        $c->email = filter_var($app->request->post('email'),FILTER_SANITIZE_STRING);
        $c->nom = filter_var($app->request->post('nom'),FILTER_SANITIZE_STRING);
        $c->prenom = filter_var($app->request->post('prenom'),FILTER_SANITIZE_STRING);
        $c->mdp = password_hash(filter_var($app->request->post()['password'],FILTER_SANITIZE_STRING), PASSWORD_BCRYPT);
        $c->permis = filter_var($app->request->post('permis'),FILTER_SANITIZE_STRING);
        $c->tel = filter_var($app->request->post('tel'),FILTER_SANITIZE_STRING);

        $mail = new \PHPMailer();
        $lien = "http://localhost" . $app->urlFor("confirm", array('email' => $c->email));
        $body = "Bonjour $c->nom $c->prenom, vous venez de vous inscrire sur autovoiture.<br/>
    Merci de confirmer votre e-mail en cliquant sur ce <a href=\"$lien\">lien</a>;";

        $mail->isSMTP();
        $mail->SMTPDebug = 4;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        //$mail->Host  = gethostbyname("smtp.openmailbox.org");
        $mail->SMTPAuth = true;
        $mail->Username = "pts4bis@gmail.com";
        $mail->Password = "Plop25Polp";
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->Host  = 'smtp.gmail.com';
        
        $mail->From = "pts4bis@gmail.com";
        $mail->FromName = "AutoVoiture";
        $mail->addAddress($c->email, "$c->nom $c->prenom");
        $mail->isHTML(true);

        $mail->Subject = 'Confirmation d\'inscription';
        $mail->Body = $body;

        if(!$mail->send()){
            echo "ERROR : $mail->ErrorInfo";
        }
        else{
            $c->save();

            $_SESSION['user'] = $c;
            $url = $app->urlFor('base');
            $app->redirect($url);
        }
    }

    public function connexion(){
        session_start();
        $app = \Slim\Slim::getInstance();
        $url=$app->urlFor("base");
        $urlMembre=$app->urlFor("baseMembre");
        $urlAdmin=$app->urlFor("baseAdmin");
        $email = filter_var($app->request->post()['email'],FILTER_SANITIZE_STRING);
        $passwd = filter_var($app->request->post()['mdp'],FILTER_SANITIZE_STRING);
        $usr = \projet_tut\models\Client::where("email", "=", $email)->get();
        $admin_mdp = \projet_tut\models\Client::where("email", "=", "admin@autovoiture.fr")->get()->first()->mdp;
        print_r($admin_mdp);
        foreach($usr as $u){
            if(password_verify($passwd, $u->mdp) && !$u->verifie){
                $_SESSION['usermail'] = $email;
                if($u->mdp == $admin_mdp){
                    $app->redirect($urlAdmin);                    
                }
                else{
                    $app->redirect($urlMembre);                    
                }
            }
            else {
                    $app->redirect($url);
            }
        }
    }

    public function confirm($email){
        $c = Client::where('email', '=', $email) -> get() -> first();

        $c->valide = true;

        $c->save();

        $vue = new VueVisiteur();
        $vue->render(2);
    }

    public function deconnexion(){
        session_start();
        $_SESSION = array();
        $app = \Slim\Slim::getInstance();
        $app->redirect("http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']));
    }

    public function planning(){
        $vue = new VueVisiteur();
        $vue->render(3);
    }

    public function reservation(){
        $vue = new VueVisiteur();
        $vue->render(4);
    }

    public function reservation_verif(){
        //TODO ajouter les vehicules
      
        session_start();
        $app = \Slim\Slim::getInstance();
        $start = filter_var($app->request->post('start'),FILTER_SANITIZE_NUMBER_INT);
        $stop = filter_var($app->request->post('stop'),FILTER_SANITIZE_NUMBER_INT);

        $r = new Reservation();
        $r->email = $_SESSION['usermail'];
        $r->start = $start;
        $r->stop = $stop;
        $r->num_vehicule = 0;
        $r->valide= null;

        $r->save();
    }

    public function demandes(){
        $vue = new VueVisiteur();
        $vue->render(5);
    }

    public function valide(){
        $app = \Slim\Slim::getInstance();
        $vue = new VueVisiteur();
        $email= filter_var($app->request->get('email'),FILTER_SANITIZE_STRING);
        $id= filter_var($app->request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        //echo $email;
        //echo $id;
        $demandes = Reservation::where('valide', '=', '1') -> get();

        $r = Reservation::where('id', '=', $id) -> get() -> first();
        $r->valide = true;
        $r->save();

        $file = "events.json.php";
        //$json = $json_decode(file_get_contents($file), true);
        $json = array();

        foreach($demandes as $d){
            //$v = Vehicule::where('')
            $json[] = array(
                'id' => $d->id,
                'title' => $d->email + "  :  ",
                'class' => 'event-important',
                'start' => $d->start,
                'end' => $d->stop,
            );

        }

        file_put_contents($file, json_encode(array('success' => 1, 'result' => $json)));

        //echo $r->valide;
        //$vue->render(5);
    }

    public function refuse(){
        $app = \Slim\Slim::getInstance();
        $vue = new VueVisiteur();
        $email= filter_var($app->request->get('email'),FILTER_SANITIZE_STRING);
        $id= filter_var($app->request->get('id'),FILTER_SANITIZE_NUMBER_INT);
        //echo $email;
        $r = Reservation::where('id', '=', $id) -> get() -> first();
        $r->valide = False;
        $r->save();
        
        
        //$vue->render(5);
    }

    public function gestion_vehicule(){
        $vue = new VueVisiteur();
        $vue->render(6);        
    }

    public function liste_vehicule(){
        $vue = new VueVisiteur();
        $vue->render(7);                
    }

    public function ajout_vehicule(){
        $vue = new VueVisiteur();
        $vue->render(8);        
    }

}
