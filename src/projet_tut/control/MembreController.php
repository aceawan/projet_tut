<?php

namespace projet_tut\control;

use Illuminate\Database\QueryException;
use projet_tut\models\Client;
use projet_tut\models\Reservation;
use projet_tut\views\VueVisiteur;
use projet_tut\views\VueMembre;

class MembreController extends AbstractController {
    
    public function index(){
        $vue = new VueMembre();
        $vue->render(0);                 
    }
    
    public function confirm($email){
        $c = Client::where('email', '=', $email) -> get() -> first();

        $c->valide = true;

        $c->save();

        $vue = new VueMembre();
        $vue->render(1);
    }

    public function deconnexion(){
        session_start();
        $_SESSION = array();
        $app = \Slim\Slim::getInstance();
        $app->redirect("http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']));
    }

    public function planning(){
        $vue = new VueMembre();
        $vue->render(2);
    }

    public function reservation(){
        $vue = new VueMembre();
        $vue->render(3);
    }

    public function liste_reservation(){
        $vue = new VueMembre();
        $vue->render(4);
    }

    public function supprimerReservation(){
        $app = \Slim\Slim::getInstance();
        $url = $app->urlFor("liste_reservation");
        $id = filter_var($app->request->get('id'),FILTER_SANITIZE_NUMBER_INT);

        $res = Reservation::find($id);
        if(!is_null($res)) {
            $res->delete();
        }
        $app->redirect($url);
    }

    public function reservation_verif(){
      
        session_start();
        $app = \Slim\Slim::getInstance();
        $start = filter_var($app->request->post('start'),FILTER_SANITIZE_NUMBER_INT);
        $stop = filter_var($app->request->post('stop'),FILTER_SANITIZE_NUMBER_INT);
        $vehicule = filter_var($app->request->post('vehicule'),FILTER_SANITIZE_NUMBER_INT);

        $res = Reservation::where('num_vehicule', '=', $vehicule)
            ->where('start', '=', $start)->where('stop', '=', $stop)
            ->orWhere(function($query) use($app, $start, $stop){
                $query->where('start', '>', $start)
                      ->Where('start', '<', $stop);})
            ->orWhere(function($query) use($app, $start, $stop){
                $query->where('start', '<', $start)
                      ->Where('stop', '>', $stop);})
            ->orWhere(function($query) use($app, $start, $stop){
                $query->where('start', '>', $start)
                      ->Where('stop', '<', $stop);})
            ->orWhere(function($query) use($app, $start, $stop){
                $query->where('stop', '>', $start)
                    ->Where('stop', '<', $stop);
            })->get();

        if(empty($res[0])){
            $r = new Reservation();
            $r->email = $_SESSION['usermail'];
            $r->start = $start;
            $r->stop = $stop;
            $r->num_vehicule = $vehicule;
            $r->valide = null;
            $r->save();
            echo "Votre réservation a été transmise avec succès à l'administrateur qui validera ou non votre demande.";
        }else{
            echo "Votre réservation n'a pas aboutie car le véhicule choisi n'est pas disponible pour ces dates";
        }
    }
}
