<?php

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use projet_tut\control\VisiteurController;
use projet_tut\control\AdminController;
use projet_tut\control\MembreController;

$db = new DB();
$db->addConnection(parse_ini_file('conf/db.projet_tut.conf.ini'));

$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

//==============================================================Pages de base===================================================================
$app->get('/', function() use($app){
        $vc = new VisiteurController($app->request);
        $vc->index();
    }) -> name("base");

$app->get('/membre', function() use($app){
        $vc = new MembreController($app->request);
        $vc->index();
    }) -> name("baseMembre");

$app->get('/admin', function() use($app){
        $ac = new AdminController($app->request);
        $ac->index();
    }) -> name("baseAdmin");
//==============================================================Pages de base===================================================================




//================================================================Visiteur======================================================================
$app->post('/auth', function() use($app){
        $vc = new VisiteurController($app->request);
        $vc->connexion();
    }) -> name("auth");

$app->get('/insc', function() use($app){
        $vc = new VisiteurController($app->request);
        $vc->form_insc();
    }) -> name("insc");

$app->post('/insc', function() use($app){
        $vc = new VisiteurController($app->request);
        $vc->inscription();
    });
//================================================================Visiteur======================================================================






//=================================================================Membre=======================================================================
$app->get('/confirm/:email', function($email) use($app){
        $vc = new MembreController($app->request);
        $vc->confirm($email);
    }) -> name('confirm');

$app->get('/deco', function() use($app){
        $vc = new MembreController($app->request);
        $vc->deconnexion();
    }) -> name("deco");

$app->get('/planning', function() use($app){
        $vc = new MembreController($app->request);
        $vc->planning();
    }) -> name("planning");

$app->get('/reservation', function() use($app){
        $vc = new MembreController($app->request);
        $vc->reservation();
    }) -> name("reservation");

$app->get('/liste_reservation', function() use($app){
    $vc = new MembreController($app->request);
    $vc->liste_reservation();
}) -> name("liste_reservation");

$app->get('/liste_reservation/supprimer', function() use($app){
    $vc = new MembreController($app->request);
    $vc->supprimerReservation();
}) -> name('suppr_reservation');

$app->post('/reservation-send', function() use($app){
        $req = $app->request();
        $vc = new MembreController($app->request);
        $vc->reservation_verif();

        //TODO ajouter les vehicules
});
//=================================================================Membre=======================================================================


//=================================================================Admin========================================================================
$app->get('/demandes', function() use($app){
        $ac = new AdminController($app->request);
        $ac->demandes();
    }) -> name("demandes");

$app->get('/valide', function() use($app){
        $req = $app->request();
        $ac = new AdminController($app->request);
        $ac->valide();
    }) -> name('valide');

$app->get('/refuse', function() use($app){
        $req = $app->request();
        $ac = new AdminController($app->request);
        $ac->refuse();
    }) -> name('refuse');

$app->get('/gestion_vehicule', function() use($app){
        $ac = new AdminController($app->request);
        $ac->gestion_vehicule();
    }) -> name('gestion_vehicule');

$app->get('/gestion_vehicule/liste', function() use($app){
    $ac = new AdminController($app->request);
    $ac->liste_vehicule();
}) -> name('gestion_vehicule_liste');

$app->get('/gestion_vehicule/supprimer_rev', function() use($app){
    $ac = new AdminController($app->request);
    $ac->supprimerVehicule_rev();
}) -> name('gestion_vehicule_supprimer_rev');



$app->get('/gestion_vehicule/changer_statut_rev', function() use($app){
    $ac = new AdminController($app->request);
    $ac->changerStatut_rev();
}) -> name('gestion_vehicule_changer_statut_rev');

$app->get('/gestion_vehicule/supprimer', function() use($app){
    $ac = new AdminController($app->request);
    $ac->supprimerVehicule();
}) -> name('gestion_vehicule_supprimer');

$app->get('/gestion_vehicule/changer_statut', function() use($app){
    $ac = new AdminController($app->request);
    $ac->changerStatut();
}) -> name('gestion_vehicule_changer_statut');

$app->get('/gestion_vehicule/liste_revision', function() use($app){
    $ac = new AdminController($app->request);
    $ac->liste_vehicule_revision();
}) -> name('gestion_vehicule_liste_revision');

$app->get('/gestion_vehicule/form_ajout', function() use($app){
        $ac = new AdminController($app->request);
        $ac->form_ajout_vehicule();
    }) -> name('gestion_vehicule_form_ajout');

$app->post('/gestion_vehicule/ajout', function() use($app){
    $ac = new AdminController($app->request);
    $ac->ajout_vehicule();
}) -> name('gestion_vehicule_ajout');


$app->get('/gestion_client', function() use($app){
        $ac = new AdminController($app->request);
        $ac->gestion_client();
    }) -> name('gestion_client');

$app->get('/gestion_client_supprimer', function() use($app){
        $ac = new AdminController($app->request);
        $ac->supprimer_client();
    }) -> name('gestion_client_supprimer');

//=================================================================Admin========================================================================


$app->run();
