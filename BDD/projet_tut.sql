-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Jeu 29 Octobre 2015 à 14:35
-- Version du serveur :  5.5.42
-- Version de PHP :  5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `projet_tut`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `email` varchar(50) NOT NULL,
  `mdp` varchar(512) NOT NULL,
  `prenom` varchar(40) NOT NULL,
  `nom` varchar(40) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `permis` varchar(10) NOT NULL,
  `valide` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`email`, `mdp`, `prenom`, `nom`, `tel`, `permis`, `valide`) VALUES
('a@gmail.com', '$2y$10$N7nbOg0Hm1hFx/sP7k4AVOh2ERSqVEiV6hldEA.wh1weSoTEzyL9K', 'a', 'a', '0', '', NULL),
('admin@autovoiture.fr', '$2y$10$hVu4AwU22yqk6A4dmG914urE8CY0dnIiuK.VXW9q6FKoIKQDL22De', 'toor', 'root', '0', '', NULL),
('belette.furtive@gmail.com', '$2y$10$tyb4P1cax4tTTuRL8ZnMdeLLBhMEPmo9phASnb6UYbvZWbA2HfNlq', 'a', 'a', '00', '', NULL),
('c@openmailbox.org', '$2y$10$fndQgV76Jb7EWGMX4nMfte5sKLeve/Ef.okgEmc3ahGbv9cXCwCtq', 'b', 'a', '0', '', NULL),
('mattassedu88@hotmail.fr', '$2y$10$jERd0hvh/0NW7ufALaD/QOvGZYEQVTn6uQluEvQJWS6CjCugmIu8G', 'matthieu', 'vincent', '09090909090', 'b', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `email` varchar(50) NOT NULL,
  `start` varchar(15) NOT NULL,
  `stop` varchar(15) NOT NULL,
  `num_vehicule` int(11) NOT NULL,
  `valide` tinyint(1) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `reservation`
--

INSERT INTO `reservation` (`email`, `start`, `stop`, `num_vehicule`, `valide`, `id`) VALUES
('mattassedu88@hotmail.fr', '1444082400000', '1444255200000', 1, NULL, 17);

-- --------------------------------------------------------

--
-- Structure de la table `vehicule`
--

CREATE TABLE `vehicule` (
  `num_vehicule` int(11) NOT NULL,
  `nb_km` int(11) NOT NULL,
  `nb_chauffeur` int(11) NOT NULL,
  `km_revision` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `revision` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `vehicule`
--

INSERT INTO `vehicule` (`num_vehicule`, `nb_km`, `nb_chauffeur`, `km_revision`, `age`, `type`, `revision`) VALUES
(5, 124, 4, 34534, 23, 'Xara', 0),
(7, 12334, 2, 200, 3, 'Clio', 0),
(8, 45689, 5, 1000, 12, '206', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`email`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vehicule`
--
ALTER TABLE `vehicule`
  ADD PRIMARY KEY (`num_vehicule`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `vehicule`
--
ALTER TABLE `vehicule`
  MODIFY `num_vehicule` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;