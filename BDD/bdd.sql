CREATE TABLE `projet_tut`.`client` ( `email` VARCHAR(50) NOT NULL , `mdp` VARCHAR(512) NOT NULL , `prenom` VARCHAR(40) NOT NULL , `nom` VARCHAR(40) NOT NULL , `tel` VARCHAR(20) NOT NULL , `permis` VARCHAR(10) NOT NULL , PRIMARY KEY (`email`(50))) ENGINE = InnoDB;

ALTER TABLE `client` ADD `valide` BOOLEAN NULL AFTER `permis`;

CREATE TABLE `reservation` (
  `email` varchar(50) NOT NULL,
  `start` varchar(15) NOT NULL,
  `stop` varchar(15) NOT NULL,
  `num_vehicule` int(11) NOT NULL,
  `valide` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
